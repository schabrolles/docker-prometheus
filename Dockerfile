FROM golang AS COMPILER


RUN go get github.com/prometheus/prometheus/cmd/...
WORKDIR  /go/src/github.com/prometheus/prometheus

ARG PROMETHEUS_VERSION
RUN git checkout v${PROMETHEUS_VERSION:-HEAD} && \
    make build


FROM busybox

RUN mkdir -p /etc/prometheus && mkdir -p /prometheus

COPY --from=COMPILER /go/src/github.com/prometheus/prometheus/prometheus /bin/prometheus
COPY --from=COMPILER /go/src/github.com/prometheus/prometheus/promtool /bin/promtool
COPY --from=COMPILER /go/src/github.com/prometheus/prometheus/documentation/examples/prometheus.yml /etc/prometheus/prometheus.yml
COPY --from=COMPILER /go/src/github.com/prometheus/prometheus/console_libraries/ /etc/prometheus/console_libraries/
COPY --from=COMPILER /go/src/github.com/prometheus/prometheus/consoles/ /etc/prometheus/consoles/

EXPOSE 9090 
VOLUME     [ "/prometheus" ]
WORKDIR    /prometheus
ENTRYPOINT [ "/bin/prometheus" ]
CMD        [ "--config.file=/etc/prometheus/prometheus.yml", \
             "--storage.tsdb.path=/prometheus", \
             "--web.console.libraries=/etc/prometheus/console_libraries", \
             "--web.console.templates=/etc/prometheus/consoles" ]